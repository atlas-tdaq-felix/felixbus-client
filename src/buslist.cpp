
#include <iomanip>

#include "docopt/docopt.h"

#include "felixtag.h"

#include "felixbus/bus.hpp"

#include "felixbus/elinktable.hpp"
#include "felixbus/felixtable.hpp"
#include "felixbus/processtable.hpp"

using namespace felix::bus;
using namespace std;

static const char USAGE[] =
R"(felix-buslist - Lists registrations on the FELIX Bus

    Usage:
      felix-buslist [options]

    Options:
      -e, --elinks                Print list by elinks
      -h, --help                  Display this help
      -g, --group GROUP_NAME      Use a different group for this bus [default: FELIX]
      -i, --interface INTERFACE   Network interface to use [default: ZSYS_INTERFACE]
      -j, --json                  Print tables in json format (for -t)
      -m, --monitoring            Print the monitoring table.
      -s, --sleep TIME            Sleep in seconds [default: 2]
      -q, --quiet                 No big printouts
      -t, --tables                Print all tables (elinks, felix)
      -V, --version               Display version
      -v, --verbose               Run in verbose mode
      -z, --zyre-verbose          Run in Zyre verbose mode
)";

int main(int argc, char *argv[]) {

  std::map<std::string, docopt::value> args = docopt::docopt(USAGE,
                                                { argv + 1, argv + argc },
                                                true,      // show help if requested
                                                FELIX_TAG);  // version string

  FelixTable felixTable;
  ElinkTable elinkTable;
  ProcessTable processTable;
  FelixTable monitTable;

  Bus bus;
  bus.setGroupName(args["--group"].asString());
  bus.setInterface(args["--interface"].asString());
  bus.setVerbose(args["--verbose"].asBool());
  bus.setZyreVerbose(args["--zyre-verbose"].asBool());

  bus.subscribe("FELIX", &felixTable);
  bus.subscribe("ELINKS", &elinkTable);
  bus.subscribe("PROCESS", &processTable);
  if (args["--monitoring"].asBool()) {
    bus.subscribe("MONITORING", &monitTable);
  }

  bus.connect();

  cout << "Tables in FelixBus (ctrl\\ to quit)" << endl;

  long counter = 0;
  while (true) {
    cout << "Update " << counter++ << endl;
    if (args["--elinks"].asBool()) {
      cout << "Elink    " << "Address     " << "PubSub " << "Unbuffered  " << "Pages  " << "PageSize  " << "user@hostname:pid  " << endl;
      set<ulong> links = elinkTable.getLinks();
      for (set<ulong>::iterator it = links.begin(); it != links.end(); it++) {
        uint elink = *it;
        string felixId = elinkTable.getFelixId(elink);
        bool pubsub = felixTable.isPubSub(felixId);
        bool unbuffered = felixTable.isUnbuffered(felixId);
        uint netio_pages = felixTable.getNetioPages(felixId);
        uint netio_pagesize = felixTable.getNetioPageSize(felixId);
        string address = felixTable.getAddress(felixId);
        string user_hostname_pid = processTable.getUserHostnamePid(felixId);
        cout << setw(5) << elink << "    " << address << "    " << pubsub << "    " << unbuffered << "    " << netio_pages << "    " << netio_pagesize << "    " << user_hostname_pid << endl;
      }
      cout << endl;
    }

    if (args["--tables"].asBool()) {
    // both tables
      if (args["--json"].asBool()) {
        json o;
        o["process"] = processTable.toJson();
        o["felix"] = felixTable.toJson();
        o["elink"] = elinkTable.toJson();
        cout << setw(2) << o << endl;
      } else {
        processTable.print();
        cout << endl;

        felixTable.print();
        cout << endl;

        elinkTable.print();
        cout << endl;
      }
    }

    if (args["--monitoring"].asBool()) {
      // the monitTable
      if (args["--json"].asBool()) {
        json o;
        o["monitoring"] = monitTable.toJson();
        cout << setw(2) << o << endl;
      } else {
        monitTable.print();
        cout << endl;
      }
    }
    sleep(args["--sleep"].asLong());
  }

  return 0;
}
