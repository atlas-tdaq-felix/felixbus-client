
#include <iomanip>

#include "docopt/docopt.h"

#include "felixtag.h"

#include "felixbus/bus.hpp"

#include "felixbus/elinktable.hpp"
#include "felixbus/felixtable.hpp"
#include "felixbus/processtable.hpp"

using namespace felix::bus;
using namespace std;

static const char USAGE[] =
R"(felix-busclient - Fakes registrations on the FELIX Bus

    Usage:
      felix-client [options]

    Options:
      -e, --elinks NUMBER  Number of elinks to publish (ports are fake) [default: 10]
      -h, --help           Display this help
      -i, --interface INTERFACE   Network interface to use [default: ZSYS_INTERFACE]
      -o, --offset NUMBER  Offset for elinks [default: 0]
      -s, --sleep TIME     Sleep in seconds before exit [default: 0]
      -V, --version        Display version
      -v, --verbose        Run in verbose mode
      -z, --zyre-verbose   Run in Zyre verbose mode
)";

int main(int argc, char *argv[]) {

  map<string, docopt::value> args = docopt::docopt(USAGE,
                                        { argv + 1, argv + argc },
                                        true,      // show help if requested
                                        FELIX_TAG);  // version string

  ProcessTable processTable;
  FelixTable felixTable;
  ElinkTable elinkTable;
  FelixTable monitTable;

  string ip = getIpAddress().second;
  cout << "Publishing Tables in FelixBus for " << ip << " (ctrl\\ to quit)" << endl;

  string address = "tcp://" + ip + ":53467";
  cout << "Address " << address << endl;
  string uuid = felixTable.addFelix(address);

  processTable.addProcess(uuid);

  long offset = args["--offset"].asLong();
  cout << "Elink offset " << offset << endl;
  for (int elink=1; elink <= args["--elinks"].asLong(); elink++) {
    elinkTable.addElink(offset + elink, uuid);
  }

  Bus bus;
  bus.setInterface(args["--interface"].asString());
  bus.setVerbose(args["--verbose"].asBool());
  bus.setZyreVerbose(args["--zyre-verbose"].asBool());

  bus.publish("PROCESS", processTable);
  bus.publish("FELIX", felixTable);
  bus.publish("ELINKS", elinkTable);

  bus.connect();

  long sleepTime = args["--sleep"].asLong();
  if (sleepTime > 0) {
    cout << "Sleeping " << sleepTime << " seconds." << endl;
    sleep(sleepTime);
  } else {
    while(true) {
      cout << "Sleeping forever." << endl;
      sleep(100);
    }
  }

  bus.disconnect();

  return 0;
}
