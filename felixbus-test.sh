#!/bin/sh
echo "Start $HOSTNAME"
cd public/felixbus-test
source cmake_tdaq/bin/setup.sh x86_64-slc6-gcc62-opt
cd x86_64-slc6-gcc62-opt/felixbus-client
./felix-busclient --elinks $1 --sleep $2 --offset $3
echo "End $HOSTNAME"
